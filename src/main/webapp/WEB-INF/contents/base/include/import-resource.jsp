<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="./common-header.jsp" %>
<!-- EasyUI 插件-->
<link href="${pageContext.request.contextPath}/plugins/easyui/themes/default/tabs.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/easyui-bootstrap.css" rel="stylesheet" />
<script src="${pageContext.request.contextPath}/plugins/easyui/plugins/jquery.parser.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/plugins/easyui/plugins/jquery.resizable.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/plugins/easyui/plugins/jquery.panel.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/plugins/easyui/plugins/jquery.linkbutton.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/plugins/easyui/plugins/jquery.tabs.js" type="text/javascript"></script>

<!-- 封装 bootstrap 弹出对话框 -->
<link href="${pageContext.request.contextPath}/css/bootstrap-dialog.css" rel="stylesheet" />
<script src="${pageContext.request.contextPath}/js/bootstrap-dialog.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap-dialog-util.js" type="text/javascript"></script>

<!-- 日期插件  -->
<link href="${pageContext.request.contextPath}/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
<script src="${pageContext.request.contextPath}/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" type="text/javascript" ></script>
<script src="${pageContext.request.contextPath}/plugins/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js" charset="UTF-8" type="text/javascript" ></script>
<!-- 上传文件插件 -->
<link href="${pageContext.request.contextPath}/plugins/jqueryFileUpload/css/jquery.fileupload.css" rel="stylesheet"/>
<link href="${pageContext.request.contextPath}/plugins/jqueryFileUpload/css/jquery.fileupload-ui.css" rel="stylesheet"/>
<script src="${pageContext.request.contextPath}/plugins/jqueryFileUpload/js/vendor/jquery.ui.widget.js"></script>
<script src="${pageContext.request.contextPath}/plugins/jqueryFileUpload/js/jquery.iframe-transport.js" type="text/javascript" ></script>
<script src="${pageContext.request.contextPath}/plugins/jqueryFileUpload/js/jquery.fileupload.js" type="text/javascript" ></script>

<!-- comet4j插件 -->
<script src="${pageContext.request.contextPath}/plugins/comet4j/comet4j-0.1.7.min.js"></script>
<link href="${pageContext.request.contextPath}/plugins/comet4j/message-push.css" rel="stylesheet">
<script src="${pageContext.request.contextPath}/plugins/comet4j/message-push.js"></script>

<!-- echarts插件 -->
<script src="${pageContext.request.contextPath}/plugins/echarts/echarts.min.js"></script>
<script src="${pageContext.request.contextPath}/plugins/echarts/theme/shine.js"></script>

<!-- 打印 -->
<link href="${pageContext.request.contextPath}/plugins/printArea/css/jquery.printarea.css" rel="stylesheet"/>
<script src="${pageContext.request.contextPath}/plugins/printArea/js/jquery.printarea.js" type="text/javascript" ></script>
<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" />

<!-- zTree插件 -->
<link href="${pageContext.request.contextPath}/plugins/zTree/css/zTreeStyle.css" type="text/css" rel="stylesheet"  />
<script src="${pageContext.request.contextPath}/plugins/zTree/js/jquery.ztree.core-3.5.min.js" type="text/javascript" ></script>
<script src="${pageContext.request.contextPath}/plugins/zTree/js/jquery.ztree.excheck-3.5.min.js" type="text/javascript" ></script>
<script src="${pageContext.request.contextPath}/plugins/zTree/js/jquery.ztree.exedit-3.5.min.js" type="text/javascript" ></script>
<script src="${pageContext.request.contextPath}/plugins/zTree/js/jquery.ztree.exhide-3.5.min.js" type="text/javascript" ></script>

<!-- jqGrid表格插件-->
<link href="${pageContext.request.contextPath}/plugins/jqGrid/css/ui.jqgrid.css" rel="stylesheet"/>
<link href="${pageContext.request.contextPath}/css/jqGrid-bootstrap.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.request.contextPath}/plugins/jqGrid/js/i18n/grid.locale-cn.js" type="text/javascript" ></script>
<script src="${pageContext.request.contextPath}/plugins/jqGrid/js/jquery.jqGrid.min.js" type="text/javascript" ></script>
<script src="${pageContext.request.contextPath}/plugins/jqGrid/js/jquery.jqGrid.bootstrap.js" type="text/javascript" ></script>

<script src="${pageContext.request.contextPath}/js/lunar-calendar.js" type="text/javascript"></script>
<!-- 自定义样式 -->
<link href="${pageContext.request.contextPath}/css/ztree-rewrite.css" type="text/css" rel="stylesheet"  />
<link href="${pageContext.request.contextPath}/css/bootstrap-extend.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/bootstrap-rewrite.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/jquery-ui-rewrite.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/common.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/cnoj-ui.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/layout.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/file-upload-util.css" rel="stylesheet" />
<!-- 与流程相关的样式 -->
<link href="${pageContext.request.contextPath}/plugins/flow/css/flow.css" rel="stylesheet" />

<!-- 自定义js -->
<script src="${pageContext.request.contextPath}/js/check-card-no.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/check-form.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/ztree-util.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jqGrid-util.js" type="text/javascript" ></script>
<script src="${pageContext.request.contextPath}/js/input-select.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/auto-complete.js" type="text/javascript"></script>

<script src="${pageContext.request.contextPath}/js/jquery-fileupload-util.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/cnoj.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/table-async-tree.js" type="text/javascript"></script>

<script src="${pageContext.request.contextPath}/js/cnoj.event.listener.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/adjust-ie-height.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquery.autotextarea.js" type="text/javascript" charset="UTF-8"></script>
